<?php

require_once 'Table.php';

class ProductTable extends Table
{
  protected $attributes = [
      'id' => 'int',
      'title' => 'varchar(255)',
      'text' => 'varchar(255)',
      'date' => 'datetime',
  ];
  protected $parametersString = "DEFAULT CHARACTER SET utf8 ENGINE=InnoDB";
}