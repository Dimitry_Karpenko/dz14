<?php
require_once 'traits/DbConnect.php';

abstract class Table
{
  use DbConnect;

  protected $attributes = [];
  protected $parametrString = 'DEFAULT CHARACTER SET utf8 ENGINE=InnoDB';

  static public function getTableName()
  {
    $className = get_called_class();
    $className = strtolower($className);
    $className = str_replace('table', '', $className).'s';

    return $className;
  }

  protected function getAttributesString()
  {
    if (!$this->attributes)
    {
      throw new Exception('No attributes for creating the Table');
    }
    else
      {
        $attributeString = '';

        foreach ($this->attributes as $key => $value)
        {
          $attributeString .= $key.' '.$value.', ';
        }

        $attributeString = substr_replace($attributeString, '', -2);

        return $attributeString;
      }

  }

  public function getParametrString()
  {
    return $this->parametrString;
  }

  public function createTable()
  {
    $sql = 'CREATE TABLE '.static::getTableName().' ('.static::getAttributesString().') '.static::getParametrString();

    static::getDb()->exec($sql);

    echo 'Таблица '.static::getTableName().' создана <br>';
  }

}