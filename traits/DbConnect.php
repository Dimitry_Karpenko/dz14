<?php

trait DbConnect
{
  protected static $db = NULL;
  protected static $dbHost = 'localhost';
  protected static $dbUser = 'root';
  protected static $dbPassword = '';
  protected static $dbName = 'shop';

  protected static function setDb()
  {
    try
    {
      $dsn = 'mysql:host='.self::$dbHost.';dbname='.self::$dbName;
      $pdo = new PDO($dsn, self::$dbUser, self::$dbPassword);
      $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $pdo->exec('SET NAMES utf8');

      return $pdo;
    }
    catch (Exception $e)
    {
      echo 'Error connection to DB! '.$e->getCode().' Message: '.$e->getMessage();
      die('Wasn\'t able to connect to DB');
    }
  }

  protected static function getDb()
  {
    $db = self::$db;

    if(!self::$db)
    {
      $db = self::setDb();
    }

    return $db;

  }



}