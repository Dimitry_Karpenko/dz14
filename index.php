<?php
require_once 'classes/ProductTable.php';
require_once 'classes/PostTable.php';
require_once 'classes/Table.php';
require_once 'traits/DbConnect.php';

$product = new ProductTable();
$post = new PostTable();

try
{
  $product->createTable();
}
catch (Exception $e)
{
  echo 'Ошибка! '.$e->getMessage();
}

try
{
  $post->createTable();
}
catch (Exception $e)
{
  echo 'Ошибка! '.$e->getMessage();
}




